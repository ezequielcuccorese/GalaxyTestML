# **Galaxia lejana** #

Este proyecto calcula los climas en una galaxia lejana según lo descripto en: **DocumentacionEjercicio.pdf**

#**Dependencias y deploy**#

##Se utilizaron los siguientes módulos de Spring:
* spring-boot-starter-web
* spring-boot-starter-data-jpa

##Se utilizo el proyecto Lombok
https://projectlombok.org/
**Se debe instalar en el ide en caso de querer levantar el proyecto.**

## Base de datos ##
Postgresql: https://www.postgresql.org/

## Pasos para levantar el proyecto:
1. git clone https://cuccoed@bitbucket.org/cuccoed/galaxytestml.git
2. cd galaxytestml/
3. Instalar postgresql y configurar la base de dato nueva en el archivo: **application.properties**. El usuario debe tener permisos de escritura.
4. mvn spring-boot:run

Cuando levanta la app se crean datos de ejemplos según la documentación. **IMPORTANTE: En caso de no querer "dropear" la base de datos, comentar el metodo  public CommandLineRunner demo(GalaxySystemService service, Messages messages) en la clase: SolarSystemApplication.**

Hay un proceso que está configurado para correr todos los días a las 2 am. Se puede cambiar desde **application.properties**

#Request#

##En ninguno de los casos es obligatorio el uso del el ID de la galaxia. En caso de no tenerlo, lo toma por default.

* http://localhost:8080/api/galaxySystem/create?galaxyName=galaxia&sunName=sunName -> **PUT**. Body tipo aplication/json para las coordenadas del sol: {"longitude":"0.0" ,"latitude":"0.0"}
* http://localhost:8080/api/galaxySystem/addPlanet?name=planeName&distanceToSun=555&angularVelocity=5&rotationOrientation=true&galaxyId=11 -> **PUT**
* http://localhost:8080/api/galaxySystem/clima?dia=560&galaxyId=1 -> **GET**
* http://localhost:8080/api/galaxySystem/findAll -> **GET**