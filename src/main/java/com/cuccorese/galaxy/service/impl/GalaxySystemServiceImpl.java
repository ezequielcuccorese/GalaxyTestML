package com.cuccorese.galaxy.service.impl;

import com.cuccorese.galaxy.exceptions.ApiException;
import com.cuccorese.galaxy.exceptions.DayNotFoundException;
import com.cuccorese.galaxy.exceptions.FullGalaxyException;
import com.cuccorese.galaxy.exceptions.GalaxyNotFoundException;
import com.cuccorese.galaxy.exceptions.MissingPlanetsException;
import com.cuccorese.galaxy.domain.Coordinate;
import com.cuccorese.galaxy.domain.Galaxy;
import com.cuccorese.galaxy.domain.Planet;
import com.cuccorese.galaxy.domain.Star;
import com.cuccorese.galaxy.domain.Weather;
import com.cuccorese.galaxy.domain.ForecastHistory;
import com.cuccorese.galaxy.process.GalaxySystemScheduledTasks;
import com.cuccorese.galaxy.repository.ForecastHistoryRepository;
import com.cuccorese.galaxy.repository.GalaxySystemRepository;
import com.cuccorese.galaxy.service.GalaxySystemService;
import com.cuccorese.galaxy.util.geometry.GeometryUtils;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation for galaxy operation. CRUD and weather calculate.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Service
public class GalaxySystemServiceImpl implements GalaxySystemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GalaxySystemService.class);

    @Autowired
    GalaxySystemRepository galaxySystemRepository;

    @Autowired
    ForecastHistoryRepository forecastRepository;

    @Override
    public Galaxy createGalaxy(String nameGalaxy, String nameSun, Coordinate coordinatesSun) {
        Star sun = new Star(nameSun, coordinatesSun);
        Galaxy galaxy = new Galaxy(nameGalaxy, sun, new ArrayList<>());
        return galaxySystemRepository.save(galaxy);
    }

    @Override
    public Galaxy addPlanet(Long galaxyId, String namePlanet, Double distanceToSun, Double angularVelocity,
            Boolean rotation) throws ApiException {
        Galaxy galaxy = galaxyId.equals(-1L) ? this.findAll().get(0) : galaxySystemRepository.findOne(galaxyId);
        if (galaxy == null) {
            throw new GalaxyNotFoundException();
        }
        if (galaxy.getPlanets().size() == 3) {
            throw new FullGalaxyException();
        }
        Double coordX = distanceToSun * Math.cos(0) + galaxy.getSun().getCoordinate().getLongitude();

        Double coordY = distanceToSun * Math.sin(0) + galaxy.getSun().getCoordinate().getLatitude();

        Coordinate coordinates = new Coordinate(coordX, coordY);

        Planet planet = new Planet(namePlanet, distanceToSun, coordinates, angularVelocity, rotation);
        galaxy.getPlanets().add(planet);

        if (galaxy.getPlanets().size() == 3) {
            forecastRepository.save(this.setWeather(galaxy));
        }
        return galaxySystemRepository.save(galaxy);
    }

    @Override
    public ForecastHistory avanceDay(Long galaxyId) throws ApiException {
        Galaxy galaxy = galaxyId.equals(-1L) ? this.findAll().get(0) : galaxySystemRepository.findOne(galaxyId);
        if (galaxy == null) {
            throw new GalaxyNotFoundException();
        }
        if (galaxy.getPlanets().size() != 3) {
            throw new MissingPlanetsException();
        }

        galaxy.setActualDay(galaxy.getActualDay() + 1);

        galaxy.getPlanets().stream().
                map((planet) -> {
                    movePlanet(planet);
                    return planet;
                }).
                forEachOrdered((planet) -> {
                    GeometryUtils.moveTheCirclePoint(planet.getCoordinate(), planet.getDistanceToSun(), planet.
                            getAnglePosition(), galaxy.getSun().getCoordinate());
                });

        return this.setWeather(galaxy);
    }

    @Override
    public List<Galaxy> findAll() {
        return (List<Galaxy>) galaxySystemRepository.findAll();
    }

    @Override
    public ForecastHistory findForecastHistory(Integer day, Long galaxyId) throws ApiException {
        Galaxy galaxy = galaxyId.equals(-1L) ? this.findAll().get(0) : galaxySystemRepository.findOne(galaxyId);
        if (galaxy == null) {
            throw new GalaxyNotFoundException();
        }
        ForecastHistory forecastHistory
                = forecastRepository.findByGalaxyIdAndDayNumberEquals(galaxy.getId(), day);
        if (forecastHistory == null) {
            throw new DayNotFoundException();
        }
        return forecastHistory;
    }

    /**
     * Move the planet in degrees.
     *
     * @param planet
     */
    private void movePlanet(Planet planet) {
        if (planet.getRotation()) {

            planet.setAnglePosition(planet.getAnglePosition() + planet.getAngularVelocity());

            while (planet.getAnglePosition() > 360) {
                planet.setAnglePosition(planet.getAnglePosition() - 360);
            }
        } else {
            planet.setAnglePosition(planet.getAnglePosition() - planet.getAngularVelocity());

            while (planet.getAnglePosition() < 0) {
                planet.setAnglePosition(planet.getAnglePosition() + 360);
            }
        }
    }

    /**
     * Sets the current atmosphere of galaxy and counts the periods that had the
     * galaxy
     *
     * @param galaxy The galaxy for calculate the weather conditions.
     */
    private ForecastHistory setWeather(Galaxy galaxy) {
        final Coordinate coordinate1 = galaxy.getPlanets().get(0).getCoordinate();
        final Coordinate coordinate2 = galaxy.getPlanets().get(1).getCoordinate();
        final Coordinate coordinate3 = galaxy.getPlanets().get(2).getCoordinate();
        final Coordinate coordinateSun = galaxy.getSun().getCoordinate();

        Double perimeter = 0.0;
        if (GeometryUtils.threeCollinearPoints(coordinate1, coordinate2, coordinate3) && GeometryUtils.
                threeCollinearPoints(coordinate1,
                        coordinate2, coordinateSun)) {
            //Cuando los tres planetas estan alineados entre si y a su vez alineados con respecto al sol, 
            //el sistema solar experimenta un periodo de sequia.
            galaxy.setActualWeather(Weather.DROUGHT);
            galaxy.setPeriodsOfDrought((galaxy.getPeriodsOfDrought() + 1));

        } else if (GeometryUtils.threeCollinearPoints(coordinate1, coordinate2, coordinate3)) {
            //Las condiciones optimas de presion y temperatura se dan cuando los tres planetas estan
            //alineados entre si pero no estan alineados con el sol.
            galaxy.setPeriodsOfOptimalConditions(galaxy.getPeriodsOfOptimalConditions() + 1);
            galaxy.setActualWeather(Weather.OPTIMAL_CONDITIONS);

        } else if (GeometryUtils.isSunIntoTheTriangle(coordinate1, coordinate2, coordinate3, coordinateSun)) {
            //Cuando los tres planetas no estan alineados, forman entre si un triangulo. Es sabido que en el momento 
            //en el que el sol se encuentra dentro del triangulo, el sistema solar experimenta un periodo de lluvia, 
            //teniendo este, un pico de intensidad cuando el perimetro del triangulo esta en su maximo.

            perimeter = GeometryUtils.calculatePerimeter(coordinate1, coordinate2, coordinate3);

            galaxy.setPeriodsOfRain(galaxy.getPeriodsOfRain() + 1);

            if (isMaxPerimetre(galaxy, perimeter)) {
                galaxy.setActualWeather(Weather.HEAVY_RAINS);
            } else {
                galaxy.setActualWeather(Weather.RAINS);
            }
        }
        ForecastHistory forecastHistory = new ForecastHistory();
        forecastHistory.setDayNumber(galaxy.getActualDay());
        forecastHistory.setPerimetre(perimeter);
        forecastHistory.setWeather(galaxy.getActualWeather());
        forecastHistory.setGalaxy(galaxy);
        return forecastHistory;
    }

    /**
     * Check if the triangle formed is the one with the highest perimeter.
     *
     * @param galaxy
     *
     * @return true/false
     */
    private Boolean isMaxPerimetre(Galaxy galaxy, Double perimeter) {
        List<ForecastHistory> forecasts = forecastRepository.findMaxPerimetres(galaxy.getId(), perimeter);

        if (!forecasts.isEmpty()) {
            if (forecasts.get(0).getPerimetre() > perimeter) {
                return Boolean.FALSE;
            }

            forecasts.stream().filter((forecast) -> {
                LOGGER.info("Clima actual: " + forecast.getWeather());
                return (forecast.getWeather() == Weather.HEAVY_RAINS);
            }).forEach((forecast) -> {
                LOGGER.info("Cambiando clima del día : " + forecast.getDayNumber());
                forecast.setWeather(Weather.RAINS);
                forecastRepository.save(forecasts);
            });
        }

        return Boolean.TRUE;
    }

}
