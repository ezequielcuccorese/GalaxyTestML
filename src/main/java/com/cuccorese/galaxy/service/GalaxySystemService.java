package com.cuccorese.galaxy.service;

import com.cuccorese.galaxy.exceptions.ApiException;
import java.util.List;

import com.cuccorese.galaxy.domain.Coordinate;
import com.cuccorese.galaxy.domain.ForecastHistory;
import com.cuccorese.galaxy.domain.Galaxy;

/**
 * That interface create a galaxy and administered its stars
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 *
 */
public interface GalaxySystemService {

    /**
     * create a new galaxy with its sun and the coordinates of the sun in the Cartesian plane.
     *
     * @param nameGalaxia    Name of galaxy.
     * @param nameSun        Name of sun.
     * @param coordinatesSun coordinates of sun.
     *
     * @return the new galaxy created.
     */
    Galaxy createGalaxy(String nameGalaxia, String nameSun, Coordinate coordinatesSun);

    /**
     * Add a planet to galaxy.
     *
     * @param galaxyId        id to look for the galaxy
     * @param namePlanet      name of planet.
     * @param distanceToSun   the distance of planet to the sun.
     * @param angularVelocity the angular velocity of planet per day.
     * @param rotation        Direction of rotation of the planet. True: Schedule, False: Counterclockwise.
     *
     * @return the galaxy with the planet added
     *
     * @throws ApiException if the galaxy is full (3 planet) throws the exception
     */
    Galaxy addPlanet(Long galaxyId, String namePlanet, Double distanceToSun, Double angularVelocity,
            Boolean rotation) throws ApiException;

    /**
     * Go ahead one day in the galaxy. Update the weather of galaxy.
     *
     * @param galaxyId id to look for the galaxy
     *
     * @return the galaxy with the new weather conditions.
     *
     * @throws ApiException If the planets is minor to 3, throws the exceptions. The galaxy need 3 planets to calculate
     *                      its weather conditions
     */
    ForecastHistory avanceDay(Long galaxyId) throws ApiException;

    /**
     * Search for all galaxies in the system.
     *
     * @return
     */
    List<Galaxy> findAll();

    /**
     * Look for the weather that happened on that day.
     *
     * @param day      the day to look for.
     * @param galaxyId the galaxy id.
     *
     * @return The forecast.
     *
     * @throws ApiException if the galaxy or day don't exist.
     */
    ForecastHistory findForecastHistory(Integer day, Long galaxyId) throws ApiException;

}
