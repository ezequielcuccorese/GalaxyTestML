package com.cuccorese.galaxy.process;

import com.cuccorese.galaxy.config.Messages;
import com.cuccorese.galaxy.exceptions.ApiException;
import com.cuccorese.galaxy.domain.ForecastHistory;
import com.cuccorese.galaxy.repository.GalaxySystemRepository;
import com.cuccorese.galaxy.service.GalaxySystemService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * The scheduled task for calculate a ten years of forecast.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Component
public class GalaxySystemScheduledTasks {

    private static final Logger LOGGER = LoggerFactory.getLogger(GalaxySystemScheduledTasks.class);

    @Autowired
    GalaxySystemService service;

    @Autowired
    GalaxySystemRepository galaxyRepository;

    @Autowired
    Messages messages;

    @PersistenceContext
    private EntityManager entityManager;

    @Value("${hibernate.jdbc.batch_size}")
    private int batchSize;

    /**
     * Update the weather of all galaxys in the system for a ten years.
     *
     */
    @Scheduled(cron = "${instructionSchedularTime}")
    @Transactional
    public void calculateTenYearsOfWeather() {
        LOGGER.info(messages.get("proceso.iniciando"));
        //TODO Averiguar como correr el job cada 10 anios.
//        service.findAll().
//                forEach((galaxy) -> {
//                    //Add 2 days for leap years
//                    Collection<ForecastHistory> forecasts = new ArrayList<>();
//                    for (int i = 0; i < (365 * 10 + 2); i++) {
//                        try {
//                            if (i % batchSize == 0) {
//                                // Flush a batch of inserts and release memory.
//                                entityManager.flush();
//                                entityManager.clear();
//                            }
//                            persistOrMerge(service.avanceDay(galaxy.getId()));
//                        } catch (ApiException ex) {
//                            LOGGER.error(messages.get("proceso.faltanplanetas"), ex);
//                        }
//                    }
//                    galaxyRepository.save(galaxy);
//                });

    }

    private <T extends ForecastHistory> T persistOrMerge(T t) {
        if (t.getId() == null) {
            entityManager.persist(t);
            return t;
        } else {
            return entityManager.merge(t);
        }
    }

}
