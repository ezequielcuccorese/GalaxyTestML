package com.cuccorese.galaxy.exceptions;

import com.cuccorese.galaxy.util.errors.GeneralErrors;

/**
 * Throws the exception when the day is not found.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public class DayNotFoundException extends ApiException {

    public DayNotFoundException() {
        super(GeneralErrors.NOTFOUND_DAY, null);
    }

}
