package com.cuccorese.galaxy.exceptions;

import com.cuccorese.galaxy.util.errors.ApiError;

/**
 * Api exception for thorws all errors in the app.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 *
 */
public class ApiException extends Exception {

    private static final long serialVersionUID = 1104306672939965560L;

    private final ApiError errorType;

    private final String description;

    public ApiException(ApiError apiError, Throwable cause) {
        super(cause);
        this.errorType = apiError;
        this.description = apiError.getCodeMessage();
    }

    /**
     * @return the errorType
     */
    public ApiError getErrorType() {
        return errorType;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
}
