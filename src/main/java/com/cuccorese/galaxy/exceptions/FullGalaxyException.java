package com.cuccorese.galaxy.exceptions;

import com.cuccorese.galaxy.util.errors.GeneralErrors;

/**
 * Throws the exception when the galaxy is full
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 *
 */
public class FullGalaxyException extends ApiException {

    private static final long serialVersionUID = 2549832339682820467L;

    public FullGalaxyException() {
        super(GeneralErrors.FULL_GALAXY, null);
    }

}
