package com.cuccorese.galaxy.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import lombok.Data;
import org.hibernate.annotations.ListIndexBase;

/**
 * This model described a Galaxy system.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 *
 */
@Entity
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Galaxy implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    /**
     * name of galaxy
     */
    private String name;

    /**
     * The galaxy sun.
     */
    @OneToOne(cascade = {CascadeType.ALL})
    private Star sun;

    /**
     * List of planets that has galaxy.
     */
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @ListIndexBase
    @OrderColumn
    private List<Planet> planets;

    /**
     * The actual weather for the galaxy.
     */
    private Weather actualWeather;

    private Integer actualDay = 0;
    private Integer periodsOfDrought = 0;
    private Integer periodsOfRain = 0;
    private Integer periodsOfOptimalConditions = 0;

    public Galaxy() {
    }

    public Galaxy(String Name, Star sun, List<Planet> planets) {
        this.name = Name;
        this.sun = sun;
        this.planets = planets;

    }

}
