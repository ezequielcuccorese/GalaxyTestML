package com.cuccorese.galaxy.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

/**
 * This model describe a coordinate point. Latitude and Longitude in the Cartesian plane.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 *
 */
@Entity
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Coordinate implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    //x
    private Double longitude;
    //y
    private Double latitude;

    public Coordinate() {
    }

    public Coordinate(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }
}
