package com.cuccorese.galaxy.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;

/**
 * This model describe a history day in the weather.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Data
@Entity
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ForecastHistory implements Serializable {

    @Id
    @GeneratedValue
    @JsonIgnore(true)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "galaxy_id")
    @JsonBackReference
    private Galaxy galaxy;

    @JsonProperty("dia")
    private Integer dayNumber;

    @JsonIgnore(true)
    private Double perimetre = 0.0;

    @JsonProperty("clima")
    private Weather weather;
}
