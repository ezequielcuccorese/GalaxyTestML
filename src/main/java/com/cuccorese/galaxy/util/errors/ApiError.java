package com.cuccorese.galaxy.util.errors;

/**
 * Interface to be implemented for the handling of error messages and messages to the client.
 *
 * @author Ezequiel
 *
 */
public interface ApiError {

    /**
     * Define a error type.
     *
     * @return the type error.
     */
    ErrorType getErrorType();

    /**
     * Describes the error for the client side
     *
     * @return the code error for messages.
     */
    String getCodeMessage();
}
