package com.cuccorese.galaxy.util.errors;

/**
 * This enum describe the errors for applications exceptions. The nomenclature is the first letter of each word and a
 * number that continue with the numbering.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 *
 */
public enum GeneralErrors implements ApiError {

    // Described a general errors
    UNSPECTED_ERROR(ErrorType.GENERAL_ERROR, "system.error.unspected"),
    ARGUMENTOS_INVALIDOS(ErrorType.GENERAL_ERROR, "user.error.arguments"),
    // Described a system errors
    SYSTEM_ERROR(ErrorType.SYSTEM_ERROR, "system.error.generic"),
    SYSTEM_NOSTATUS(ErrorType.SYSTEM_ERROR, "system.error.noStatus"),
    SYSTEM_INVALID_MESSAGE(ErrorType.SYSTEM_ERROR, "system.error.invalid.message"),
    SYSTEM_MEDIA_TYPE(ErrorType.SYSTEM_ERROR, "system.error.media.type"),
    SYSTEM_UNSUPPORTED(ErrorType.SYSTEM_ERROR, "system.error.unsupported.method"),
    SYSTEM_REST(ErrorType.SYSTEM_ERROR, "system.error.rest"),
    // Described a business errors
    USER_INVALID_MESSAGE(ErrorType.BUSINESS_ERROR, "user.error.invalid.message"),
    USER_UNSUPPORTED(ErrorType.BUSINESS_ERROR, "user.error.unsupported.method"),
    USER_BINDING(ErrorType.BUSINESS_ERROR, "user.error.binding"),
    USER_MEDIA_TYPE(ErrorType.BUSINESS_ERROR, "user.error.media.type"),
    MISSING_GALAXY(ErrorType.BUSINESS_ERROR, "user.error.inexistent.galaxy"),
    FULL_GALAXY(ErrorType.BUSINESS_ERROR, "user.error.max.planets"),
    MISSING_PLANETS(ErrorType.BUSINESS_ERROR, "user.error.min.planets"),
    NOTFOUND_DAY(ErrorType.BUSINESS_ERROR, "user.error.notfound.day");

    private final ErrorType tipoError;

    private final String codeMessage;

    private GeneralErrors(ErrorType tipoError, String codeMessage) {
        this.tipoError = tipoError;
        this.codeMessage = codeMessage;
    }

    @Override
    public String getCodeMessage() {
        return codeMessage;
    }

    @Override
    public ErrorType getErrorType() {
        return tipoError;
    }

}
