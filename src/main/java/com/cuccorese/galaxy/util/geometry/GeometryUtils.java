/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cuccorese.galaxy.util.geometry;

import com.cuccorese.galaxy.domain.Coordinate;

/**
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public class GeometryUtils {

    /**
     * Calculate the distance from two points.
     *
     * @param p1 the other point.
     * @param p2 the other point.
     *
     * @return the distance.
     */
    private static double distanceBetweenTwoPoints(Coordinate p1, Coordinate p2) {
        return Math.sqrt(Math.pow(p1.getLatitude() - p2.getLatitude(), 2) + Math.pow(p1.getLongitude() - p2.
                getLongitude(), 2));
    }

    /**
     * Check if three points are collinear to each other.
     *
     * @param pA
     * @param pB
     * @param pC
     *
     * @return true/false
     */
    public static Boolean threeCollinearPoints(Coordinate pA, Coordinate pB, Coordinate pC) {
        double distanceAC = distanceBetweenTwoPoints(pA, pC);
        double distanceAB = distanceBetweenTwoPoints(pA, pB);
        double distanceBC = distanceBetweenTwoPoints(pB, pC);
        double maxDistance = Math.max(distanceAC, Math.max(distanceAB, distanceBC));

        return maxDistance == (distanceAC + distanceAB) ||
                maxDistance == (distanceAB + distanceBC) ||
                maxDistance == (distanceAC + distanceBC);
    }

    /**
     * Calculate the perimeter formed by a triangle of three points.
     *
     * @param p1
     * @param p2
     * @param p3
     *
     * @return the perimeter value.
     */
    public static double calculatePerimeter(Coordinate p1, Coordinate p2, Coordinate p3) {
        return distanceBetweenTwoPoints(p1, p2) + distanceBetweenTwoPoints(p1, p3) + distanceBetweenTwoPoints(p3, p2);
    }

    /**
     * Calculate the orientation (positive / negative) of the triangle.
     *
     * @param pointA
     * @param pointB
     * @param pointC
     *
     * @return the orientation
     */
    private static double orientationTriangle(Coordinate pointA, Coordinate pointB, Coordinate pointC) {
        return (pointA.getLongitude() - pointC.getLongitude()) * (pointB.getLatitude() - pointC.getLatitude()) -
                (pointA.getLatitude() - pointC.getLatitude()) * (pointB.getLongitude() - pointC.getLongitude());
    }

    /**
     * Check if the a point is internal in one triangle defined by the pontA, pointB and pointC.
     *
     *
     * @param pointA
     * @param pointB
     * @param pointC
     * @param intoPoint
     *
     * @return true/false
     */
    public static Boolean isSunIntoTheTriangle(Coordinate pointA, Coordinate pointB, Coordinate pointC,
            Coordinate intoPoint) {

        // Calculate the orientation of the triangles
        // pPlanetA-pPlanetB-pPlanetC
        Double orientationTrianglePlanets = orientationTriangle(pointA, pointB, pointC);

        // pPlanetA-pPlanetB-sun
        Double orientationABSun = orientationTriangle(pointA, pointB, intoPoint);

        // pPlanetB-pPlanetC-sun
        Double orientationBCSun = orientationTriangle(pointB, pointC, intoPoint);

        // pPlanetC-pPlanetA-sun
        Double orientationCASun = orientationTriangle(pointC, pointA, intoPoint);

        return (orientationTrianglePlanets < 0 && orientationABSun < 0 && orientationBCSun < 0 && orientationCASun < 0) ||
                (orientationTrianglePlanets >= 0 && orientationABSun >= 0 && orientationBCSun >= 0 &&
                orientationCASun >= 0);
    }

    /**
     * Moves the coordinates of the point of a circle, given its angle, center and its radius.
     *
     * @param planetCoordinate
     * @param radio
     * @param actualAngle
     * @param centralPoint
     */
    public static void moveTheCirclePoint(Coordinate planetCoordinate, Double radio, Double actualAngle,
            Coordinate centralPoint) {
        planetCoordinate.setLatitude(radio * Math.sin(Math.toRadians(actualAngle)) + centralPoint.getLatitude());
        planetCoordinate.setLongitude(radio * Math.cos(Math.toRadians(actualAngle)) + centralPoint.getLongitude());
    }
}
