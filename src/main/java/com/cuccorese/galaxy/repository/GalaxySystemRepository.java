package com.cuccorese.galaxy.repository;

import org.springframework.data.repository.CrudRepository;

import com.cuccorese.galaxy.domain.Galaxy;

public interface GalaxySystemRepository extends CrudRepository<Galaxy, Long> {

}
