package com.cuccorese.galaxy.web;

import com.cuccorese.galaxy.exceptions.ApiException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cuccorese.galaxy.domain.Coordinate;
import com.cuccorese.galaxy.domain.ForecastHistory;
import com.cuccorese.galaxy.domain.Galaxy;
import com.cuccorese.galaxy.service.GalaxySystemService;

/**
 * Input controller for create galaxy, put planets and view weather
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 *
 */
@RestController
@RequestMapping(value = "/api/galaxySystem")
public class GalaxySystemController {

    @Autowired
    GalaxySystemService galaxySystemService;

    /**
     * Create a galaxy.
     *
     * @param galaxyName
     * @param sunName        this parameter is optional
     * @param sunCoordinates
     *
     * @return a new galaxy
     */
    @ResponseBody
    @RequestMapping(value = "/create", method = RequestMethod.PUT)
    public Galaxy createGalaxy(@RequestParam(value = "galaxyName") String galaxyName,
            @RequestParam(value = "sunName", defaultValue = "Sun") String sunName,
            @RequestBody Coordinate sunCoordinates) {

        return galaxySystemService.createGalaxy(galaxyName, sunName, sunCoordinates);
    }

    /**
     * Find and list all galaxies.
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public List<Galaxy> findAll() {
        return galaxySystemService.findAll();
    }

    /**
     * Add a planet to galaxy.
     *
     * @param name
     * @param galaxyId            is optional.
     * @param distanceToSun
     * @param angularVelocity
     * @param rotationOrientation
     *
     * @return
     *
     * @throws ApiException
     */
    @ResponseBody
    @RequestMapping(value = "/addPlanet", method = RequestMethod.PUT)
    public Galaxy addPlanetToGalaxy(@RequestParam(value = "name") String name, @RequestParam(value = "galaxyId",
            defaultValue = "-1") Long galaxyId, @RequestParam(value = "distanceToSun") Double distanceToSun,
            @RequestParam(value = "angularVelocity") Double angularVelocity,
            @RequestParam(value = "rotationOrientation") Boolean rotationOrientation) throws ApiException {

        return galaxySystemService.addPlanet(galaxyId, name, distanceToSun, angularVelocity, rotationOrientation);
    }

    /**
     * Check the weather in a day.
     *
     * @param day
     * @param galaxyId is optional.
     *
     * @return
     *
     * @throws ApiException
     */
    @ResponseBody
    @RequestMapping(value = "/clima", method = RequestMethod.GET)
    public ForecastHistory weather(@RequestParam(value = "dia") Integer day, @RequestParam(value = "galaxyId",
            defaultValue = "-1") Long galaxyId) throws ApiException {
        return galaxySystemService.findForecastHistory(day, galaxyId);
    }
}
